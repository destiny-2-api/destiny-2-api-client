import Vue from 'vue'
import Router from 'vue-router'

import HomeRoute from '@/components/routes/HomeRoute'
import UserSearchRoute from '@/components/routes/UserSearchRoute'
import ProfileRoute from '@/components/routes/ProfileRoute'
import ProfileCharactersRoute from '@/components/routes/ProfileCharactersRoute'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: HomeRoute
    },
    {
      path: '/search',
      name: 'Search',
      component: UserSearchRoute,
      props: (route) => ({ q: route.query.q })
    },
    {
      path: '/profile/:membershipType/:membershipId',
      name: 'Profile',
      component: ProfileRoute,
      props: true,

      children: [
        {
          path: 'characters',
          name: 'ProfileCharacters',
          component: ProfileCharactersRoute
        }
      ]
    }
  ]
})
