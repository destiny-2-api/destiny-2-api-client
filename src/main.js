// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import { DestinyApiClient } from 'destiny-api-js'

Vue.use((VueGlobal, options) => {
  VueGlobal.prototype.$destinyApiClient = new DestinyApiClient('07c3575ae8a14ff6b13711feea469f47', 'lol')
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
